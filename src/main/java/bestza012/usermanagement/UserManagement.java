/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class UserManagement implements Serializable {

    static ArrayList<User> userList = new ArrayList<>();
    static User currentUser = null;
    static User superadmin = new User("super", "super");

    static   { //when class created this block will process first:
        load();
    }

    //CRUD
    // Create >> Insert
    static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }

    //Delete
    static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }

    //Delete
    static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    ;
    
    //Read
    static ArrayList<User> getUser() {
        return userList;
    }

    static User getUser(int index) {
        return userList.get(index);
    }

    //Update
    static boolean upateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }

    static boolean login(User user) {
        if (user.getUsername().equals("super") && user.getPassword().equals("super")) {
            return true;
        }
        for (int i = 0; i < userList.size(); i++) {
            if (user.getUsername().equals(userList.get(i).getUsername()) && user.getPassword().equals(userList.get(i).getPassword())) {
                currentUser = user;
                return true;
            }
        }
        return false;
    }

    static User getCurrentUser() {
        return currentUser;
    }

    static void logout() {
        currentUser = null;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
